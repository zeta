enable_language(CXX)

CMAKE_MINIMUM_REQUIRED(VERSION 2.4)
cmake_policy(SET CMP0003 OLD)
project(libzeta)

add_subdirectory(src)

