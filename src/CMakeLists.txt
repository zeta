add_definitions(-W -Wall -ansi -pedantic -g -Wno-long-long -Wno-unused-parameter -DZETA_USE_SDL)

include_directories(/usr/include/SDL headers)

add_library(zeta zeta.cpp)

add_executable(zetatest test/zetatest.cpp)

target_link_libraries(zetatest SDL SDL_image SDL_gfx SDL_ttf zeta)

