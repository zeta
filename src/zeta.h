#ifndef ZETA_H
#define ZETA_H

#include <iostream>
#include <string>

using std::string;

#include "zeta_begin.h"

#include "zeta_virtual.h"

#ifdef ZETA_USE_SDL
    #include "zeta_sdl.h"
#else
    #error "zeta: No library specified. Try defining ZETA_USE_SDL."
#endif

#include "zeta_factory.h"

#include "zeta_end.h"

#endif
