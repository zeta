#ifdef ZETA_FACTORY
#define ZETA_FACTORY

#ifdef ZETA_USE_SDL

class create {
public:
    static zeta_font font(string name, size_t size) {
        return zeta_font_sdl(name, size);
    }
    static zeta_interface interface() {
        return zeta_interface_sdl();
    }
};



#endif

#endif
