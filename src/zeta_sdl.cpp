#include "zeta_begin.h"

#include "zeta_sdl.h"

zeta_interface_sdl::zeta_interface_sdl() {
    if(!SDL_WasInit(SDL_INIT_VIDEO)) SDL_Init(SDL_VIDEO);
    if(!SDL_WasInit(SDL_INIT_TIMER)) SDL_Init(SDL_TIMER);
    if(!TTF_WasInit()) TTF_Init();
}

#include "zeta_end.h"