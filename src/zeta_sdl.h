#ifndef ZETA_SDL_H
#define ZETA_SDL_H

#ifndef ZETA_USE_SDL
    #error "zeta: ZETA_USE_SDL should only be used internally."
#endif

class zeta_interface_sdl : public zeta_interface {
public:
    zeta_interface_sdl();
};

#endif