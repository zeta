#ifndef ZETA_VIRTUAL
#define ZETA_VIRTUAL

class zeta_font {
public:
    zeta_font(string name, size_t size);
    ~zeta_font();
    string get_name() {return fontname;}
private:
    string fontname;
};

class zeta_interface {
public:
    zeta_interface();
    ~zeta_interface();
    virtual void draw_text(string s, int xpos, int ypos, zeta_font &font) = 0;
};

#endif